import Pkg

include("Functions.jl")
include("Plotting.jl")
include("Mechanics.jl")
include("helpers.jl")
include("CreateResults.jl")
include("Initaisation.jl")

using Profile
#using ProfileView
using LinearAlgebra
using Statistics
using Dates
using DelimitedFiles
using Distributions
using Distances
using Plots
using Printf
#using PyPlot
using Logging
using StatsPlots

#plotlyjs()
gr()
#pyplot()

@info "$(Threads.nthreads()) threads detected"

#BLASTHREADS = 2
#@info "setting BLAS threads to $BLASTHREADS"
#BLAS.set_num_threads(BLASTHREADS)

##settings
output_path="output/"

evo_params=Dict()                               # for hyperparameters
vars = Dict()                                   # for stuff that changes


evo_params[:rec] = false                        # take snapshots of evolutionary state
evo_params[:additional_readouts] = false        # record evolution of each conductivity, pressure differecnces etc.
evo_params[:draw_path] = true                   # draw conductivities in snpashots
evo_params[:allow_eary_stop] = false            # allow stopping bevor max generations are reached
load_field = true                               # load E_field from E_list
load_mask = true                                # load mask for E_field


## hyperparameters

# noise

evo_params[:E_noise_field] = false	            # Default: false
evo_params[:noise_strength] = 0.001	            # Default: 0.05 for 3x3 0.001 for 25x25
evo_params[:noise_area] = 0.3                   # Default: 0.3 for percolation threshold

evo_params[:direct_noise] = false	            # Default: false
evo_params[:direct_noise_strength] = 0.002      # Default: 0.005 for3x3

# field
evo_params[:resource_abundance] = 0.02          # if E_field is newly generated this represents the fraction of patches with resources on it
evo_params[:resource_strength] = 1.0            # 1 for 3x3, 17/6 for tero labyrinth
evo_params[:active_fraction] = 0.5              # 0.17   # fraction of active resource patches

#integration
evo_params[:max_steps] = 15000                  # Default: 15000
evo_params[:dt] = 0.001                         # Default: 0.001

# evolution influencers
evo_params[:change_period] = 1		            # Default: 1
evo_params[:gamma_sweep] = false                # Default: false
evo_params[:gamma_0] = 1.5                      # Default: 1.5
evo_params[:gamma_end] = 2.0                    # Default: 1.5 or 2.0
evo_params[:mu] = 1.			                # Default: 1.0
evo_params[:k] = 1.                             # Default: 1.0
# graph
evo_params[:L] = 3
evo_params[:N] = evo_params[:L]^2
evo_params[:init_random] = false
evo_params[:initial_caps] = 0.15		            # Default: 0.01 for 25x25 0.15 for 3x3 (NOTE: initial caps are scaled if R,k,mu are changed)
evo_params[:center_Agent] = Int(floor((evo_params[:N] + 1) / 2))

## scanning

evo_params[:scan] = false
evo_params[:scanning_parameter] = :resource_strength
evo_params[:scanning_values] = [1, 2, 4, 8, 16, 32, 64]		#collect(1/6:1/6:1.0) #[1,4,16,64,256]#collect(0.5:0.5:2.5)
evo_params[:runs_per_value] = 18



## create directory for results with log file
evo_params[:run_id]=externals.init_dir(evo_params, output_path)

## Initializing Adjacency Matrix ###
evo_params[:Adjacency_Matrix] = externals.init_Adjacency(evo_params[:L], evo_params[:N])

## init energy field

E_field = externals.set_E_field(evo_params[:N],load_field, evo_params[:resource_abundance] , evo_params[:run_id], output_path=output_path)
if evo_params[:active_fraction] * sum(E_field)  < 1
  @warn "minimum of one active field was forced"
  evo_params[:active_fraction] = round( 1.0 / sum(E_field))
end

evo_params[:resource_abundance]  = sum(E_field) / length(E_field)
evo_params[:E_field] =  E_field
vars[:dead] =  falses(evo_params[:N])
@show typeof(evo_params[:E_field])
@show evo_params[:E_field]
pressure_profile = model_mechanics.get_pressure_profile(;evo_params..., vars...)
Plotting.scatter_Energy(pressure_profile)
name = string("$output_path/$(evo_params[:run_id])/E_field_gradient.png")
savefig(name)


## Init Capacity Matrix
# WARNING: this will also connect dead agents if they exist
vars[:conductivity] = externals.init_conductivities(;evo_params...)

# Setting up activity state
vars[:p] = zeros(evo_params[:N])
dead = falses(evo_params[:N])
if load_mask
  vars[:dead] = initialisation.init_mask(evo_params,vars,output_path)
  mask = falses(length(E_field))
  mask[:] .= vars[:dead]
  vars[:mask] = mask
else
  vars[:dead] = dead
  mask = falses(length(E_field))
  vars[:mask] = mask
end
evo_params[:E_field][findall(x -> x==true,mask)] .= 0

# Disconnect dead agents
vars[:conductivity] = vars[:conductivity] .* (1 .- vars[:dead]) .* transpose(1 .- vars[:dead])


##
# TEMP: This block should be empty
###-------------------------------------------------------------------###
### manual changes for experiments                                      #
#                                                                       #
#                                                                       #
###-------------------------------------------------------------------###


## set remaining parameters
vars[:i] = 1
vars[:t] = [0.]
conductivity_history = vars[:conductivity][findall(x-> x !=0 , triu(evo_params[:Adjacency_Matrix]))]    #
vars[:conductivity_history] = conductivity_history

##
@info "start evolution"
if evo_params[:scan]
  helpers.scan(evo_params=evo_params, vars=vars, output_path=output_path)
else
  results = model_mechanics.evolve(;evo_params..., vars...)
  @info "saving results"
  CreateResults.write_final_vars(vars, evo_params[:run_id], path=output_path)
  CreateResults.write_results(results, evo_params[:run_id], path=output_path)
  CreateResults.create_plots(vars, results, evo_params, path="$(output_path)$(evo_params[:run_id])")
end


@info "done"
