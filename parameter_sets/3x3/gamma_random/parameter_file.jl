module ParameterFile
function loadFile(evo_params)
    evo_params[:resource_strength] = 1.0                
    evo_params[:active_fraction] = 1.0                 
    evo_params[:direct_noise_strength] = 0.0
    evo_params[:noise_strength] = 0.0
    evo_params[:noise_area] = 0.0
    evo_params[:max_steps] = 15000
    evo_params[:change_period] = 1
    evo_params[:gamma_0] = 1.0
    evo_params[:mu] = 1.
    evo_params[:k] = 1.
    evo_params[:init_random] = true
    evo_params[:initial_caps] = 0.15		                # NOTE: initial caps are scaled if R,k,mu are changed)
    evo_params[:scan] = true
    evo_params[:scanning_parameter] = :gamma_0
    evo_params[:scanning_values] = [0.5, 0.75, 1.0, 1.25, 1.5, 1.75, 2.0]		
    evo_params[:runs_per_value] = 18

    rescale = false

    return rescale
end
end
