module ParameterFile
function loadFile(evo_params)
    evo_params[:resource_strength] = 1.0                # 1 for 3x3, 17/6 for tero labyrinth
    evo_params[:active_fraction] = 1.0                  # 0.17   # fraction of active resource patches
    evo_params[:direct_noise_strength] = 0.0
    evo_params[:noise_strength] = 0.0
    evo_params[:noise_area] = 0.0
    evo_params[:max_steps] = 15000
    evo_params[:change_period] = 1
    evo_params[:gamma_0] = 1.75
    evo_params[:mu] = 1.
    evo_params[:k] = 1.
    evo_params[:init_random] = false
    evo_params[:initial_caps] = 0.15		                # NOTE: initial caps are scaled if R,k,mu are changed)
    evo_params[:scan] = true
    evo_params[:scanning_parameter] = :direct_noise_strength
    evo_params[:scanning_values] = [0.0; 0.005; 0.010; 0.015; 0.020; 0.025; 0.030]		#collect(1/6:1/6:1.0) #[1,4,16,64,256]#collect(0.5:0.5:2.5)
    evo_params[:runs_per_value] = 18

    rescale = false

    return rescale
end
end
