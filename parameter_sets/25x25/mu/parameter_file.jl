module ParameterFile
function loadFile(evo_params)
    evo_params[:resource_strength] = 2.0                
    evo_params[:active_fraction] = 0.5                  
    evo_params[:direct_noise_strength] = 0.0
    evo_params[:noise_strength] = 0.001
    evo_params[:noise_area] = 0.3
    evo_params[:max_steps] = 15000
    evo_params[:change_period] = 1
    evo_params[:gamma_0] = 1.5
    evo_params[:mu] = 1.
    evo_params[:k] = 1.
    evo_params[:init_random] = false
    evo_params[:initial_caps] = 0.15		                # NOTE: initial caps are scaled if R,k,mu are changed)
    evo_params[:scan] = true
    evo_params[:scanning_parameter] = :mu
    evo_params[:scanning_values] = [1; 1/2; 1/4; 1/8; 1/16]	
    evo_params[:runs_per_value] = 6

    rescale = true

    return rescale
end
end
