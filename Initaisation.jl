module initialisation

using Dates
using LinearAlgebra
using DelimitedFiles
using Statistics
using Distributed
using Distributions
using Plots
using Printf

include("Mechanics.jl")
include("Functions.jl")
include("Plotting.jl")
include("CreateResults.jl")

"""
    init_mask(evo_params, vars)

Sets activity status of node accoring to a given mask without disconnecting inactive nodes.
"""
function init_mask(evo_params, vars, output_path)
    dead=falses(evo_params[:N])
    externals.load_mask(dead)
    vars[:dead] = dead

    pressure_profile = model_mechanics.get_pressure_profile(;evo_params..., vars...)

    Plotting.scatter_Energy(pressure_profile)
    name = string("$(output_path)$(evo_params[:run_id])/constrained_pressure_profile.png")
    savefig(name)

    Plotting.scatter_Agents(evo_params[:E_field],dead)
    name = string("$(output_path)$(evo_params[:run_id])/masked_Efield.png")
    savefig(name)
    return vars[:dead]
end

end
