#!~/julia-1.3.1/bin/julia
module model_mechanics

include("Functions.jl")
include("Plotting.jl")

using Dates
using DelimitedFiles
using Distributions
using LinearAlgebra
using Logging
using Plots
using Polynomials
using Printf
using Random
using Plots
#using PyPlot
using Statistics
using StatsBase

gr()
#plotlyjs()
#pyplot()

# TODO: sort inputs: params, optional pramas (flags), vars
"""
    evolve([run_id, rec, draw_path, i ,t  ,logfile, resource_abundance, max_steps, mutation_strenght, strict_evolve, chance, skip_mutation, Agents, Adjacency_Matrix, dead ,old_dead ,conductivity ,old_conductivity ,E_field ,phase2 ,p  ,surplus, center_Agent, equillibration_steps, cost, L, leakage])

Runs evolution.
"""
function evolve(;
    run_id = run_id,
    rec = rec,
    draw_path = draw_path,
    i = i,
    t = t,
    max_steps = max_steps,
	allow_eary_stop = allow_eary_stop,
    Adjacency_Matrix = Adjacency_Matrix,
    dead = dead,
	mask = mask,
    conductivity = conductivity,
    E_noise_field = E_noise_field,
    noise_strength = noise_strength,
    noise_area = noise_area,
    resource_abundance = resource_abundance,
	resource_strength = resource_strength,
    direct_noise = direct_noise,
    direct_noise_strength = direct_noise_strength,
    E_field = E_field,
	change_period = change_period,
	gamma_sweep = gamma_sweep,
 	gamma_0 = gamma_0,
	gamma_end = gamma_end,
	mu = mu,
	k = k,
    p = p,
    center_Agent = center_Agent,
    L = L,
    dt = dt,
	conductivity_history = conductivity_history,
	active_fraction = active_fraction,
	additional_readouts = additional_readouts,
	args...
)

    evolving = true

	# Initializing results
    total_conductivity = []
	mean_distance = []
    source_pressure = []
	total_flows = []
    pressures = []
	pressure_differences = []

    stationary_flow = zeros(L*L)
    base_flow = zeros(L*L)

    phase2=false

	L_adv = get_L_adv(conductivity)
	p = get_pressure(
		L_adv = L_adv,
		solution = stationary_flow,
		center_Agent = center_Agent,
	)



	# evolution loop
    while evolving

		gamma = gamma_t(gamma_sweep,gamma_0,gamma_end,t, max_steps, phase2)

	    externals.print_progress(i, max_steps)

		#generate new resource distribution each time one change_period was completed
		if t[end] % change_period == 0
		    base_flow = set_sol(
	            source_patches = E_field,
	            dead = dead,
	            active_fields = Integer(floor(sum(E_field) * active_fraction)),
				resource_strength = resource_strength
	        )
		end

		# add addtional resources to random locations
		if E_noise_field
	       	stationary_flow = add_noise_field(sol=base_flow,fraction=noise_area,strength=noise_strength,dead=dead)
		else
		    stationary_flow = base_flow
		end

## calculations
	    update_conductivity(
	        Adjacency_Matrix = Adjacency_Matrix,
	        conductivity = conductivity,
	        p = p,
			dead=dead,
	        dt = dt,
	        gamma = gamma,
			mu = mu,
			k = k,
	        direct_noise = direct_noise,
	        direct_noise_strength = direct_noise_strength,
	    )

		# get new pressures
		L_adv = get_L_adv(conductivity)

	   	p = get_pressure(
		   L_adv = L_adv,
		   solution = stationary_flow,
		   center_Agent = center_Agent,
	   	)

## recording read-outs
# WARNING this should explicitly be stated in thesis
# resource strength is set to 1.0 and resource patches are activated, no matter what

		# sample mean_distance each 25th generation
		# resource strength set to 1.0
		if t[end] % 25 == 0
			push!(mean_distance, get_mean_distance(E_field, dead, 1.0, conductivity, center_Agent))
		end

		stationary_flow_total = set_sol(
			source_patches = E_field,
			dead = dead,
			active_fields = Integer(floor(sum(E_field))),
			resource_strength = 1.0
		)
		p_all = get_pressure(
	        L_adv = L_adv,
	        solution = stationary_flow_total,
	        center_Agent = center_Agent,
	    )

		# calculate the sum of all flows that take place in the network
		P = get_difference(conductivity,p_all)
		push!(total_flows, sum(conductivity .* abs.(P))/2)

		# record pressure at resource_patches
		push!(source_pressure, p_all[findall(x -> x > 0, E_field)])

		# record sum of all coundctivities
		push!(total_conductivity, sum(conductivity))

		# record time resolved data for pressures, conductivities, and pressure differences
		if additional_readouts
			conductivity_history = cat(dims=2, conductivity_history, conductivity[findall(x-> x !=0 , triu(Adjacency_Matrix))])
			push!(pressures,p_all)
			push!(pressure_differences, P[findall(x-> x !=0 , triu(Adjacency_Matrix))])
		end

	    ### generating plots TODO: encapsulate
	    if rec
	        if t[end] % 25 == 0 || t[end] == 0
				generation = @sprintf("%05d",t[end])
				P=get_difference(conductivity, p)				# Matrix containing Δp

				scatter(abs.(hcat(P .* conductivity...)),hcat(conductivity...),legend=false)

				# CQ -Diagramm
				x=collect(1:1000) ./ 1000
				y = (1/mu) * (x.^gamma ./ (1 .+ x.^gamma))
				plot!(x,y,xlabel="|Q|", ylabel="C", xlim=(0,1.505),ylim=(0,0.1), thickness_scaling=1.25, dpi=320)
				savefig("output/$run_id/cq/State_after_$(generation)_Generations.png")

				# PC - Diagramm
				function Pc(x,gamma); y= ((-mu*x/(mu*x-1))^(1/gamma))/x;return y;end
				x=(collect(20:500) ./ 5000) * 1 # from  4e-3 to 0.1
				y=Pc.(x,gamma)
				scatter(hcat(conductivity...),abs.(hcat(P...)),legend=false)
				plot!(x,y,xlabel="C", ylabel="Δp", xlim=(0,0.1),ylim=(0,20), thickness_scaling=1.25, dpi=320)
				savefig("output/$run_id/pc/State_after_$(generation)_Generations.png")

				# PQ - Diagram
				x = (collect(1:500) ./ 5000) * 15
				y = mu * (x + (x).^(1-gamma))
				scatter(abs.(hcat(P .* conductivity...)),abs.(hcat(P...)),legend=false)
				plot!(x,y,xlabel="|Q|", ylabel="|Δp|", xlim=(0,1.505),ylim=(0,20), thickness_scaling=1.25, dpi=320)
				savefig("output/$run_id/pq/State_after_$(generation)_Generations.png")


	            Plotting.scatter_Agents(E_field, dead)
	            if draw_path
	                Plotting.flow_path(
	                    Adjacency_Matrix,
	                    conductivity,
	                    L;
	                    overlay = true,
						# TEMP: this should not be set
						normalize_conductivity=true,
						normalisation_factor=0.22,
	                )
	            end
	            savefig("output/$run_id/states/State_of_Agents_after_$(generation)_Generations.png")
	        end
	    end

	    ### ending phase
	    if allow_eary_stop && i % 1000 == 0 && i > max_steps / 10
	        # stop if fitness gain over last 20% of max steps is lower than 0.1%
	        # FIXME: sharp fitness gain followed by moderate fitness gain may lead to early termination
	        # FIXME: use a sofisticated method to ge the slope
	        if (
	            abs((mean(total_conductivity[end-Int(max_steps / 20):end])-
	            mean(total_conductivity[end-Int(max_steps/10):end-Int(max_steps/20)]))/
	            mean(total_conductivity[end-Int(max_steps / 10):end])) <= 0.01
	        )
	            @info "finnished after ", i, " generations"
	            if phase2
					if gamma/gamma_end > 0.95 # dont stop if gamma still cahnges
						evolving = false
						@info "finished with" gamma/gamma_end
					end
	            else
	                # FIXME: disabling noise in phase2 should be handled differently
	                phase2 = true
	                i = 1
	                @info "starting phase 2"
	            end
	        end
		end
	    if i == max_steps
	        if !phase2
	            phase2 = true
	            i = 1
	        else
	            evolving = false
	        end
	    end

	    i += 1
		append!(t, t[end] + 1)                                                                 #counting generations
    end

    @info "Evolution Complete!"

	results = Dict(
	:final_pressures => p,
	:normalised_conductivity => total_conductivity ./ sum(Adjacency_Matrix) ./ (resource_strength*active_fraction*sum(E_field)),
	:mean_distance => mean_distance,
	:source_pressure => transpose(hcat(source_pressure...)),
	:total_flows => total_flows,
	:conductivity_history => transpose(conductivity_history) #transpose(hcat(conductivity_history...))
	)

	if additional_readouts
		results[:pressures] =  transpose(hcat(pressures...))
		results[:pressure_differences] = transpose(hcat(pressure_differences...))
		results[:conductivity_history] = transpose(conductivity_history)
	end

	return results

end

"""
	get_mean_distance(E_field, dead, resource_strength, conductivity, center_Agent)

Calculates the mean of the mean distances of all resource patches to all other patches.
"""
function get_mean_distance(E_field, dead, resource_strength, conductivity, center_Agent)
	source_patches = findall(x -> x > 0, E_field)
	MDs = []
	for source in source_patches
		local_E_field = zeros(length(E_field))
		local_E_field[source] = 1.
		temp_solution = set_sol(
			source_patches = local_E_field,
			dead = dead,
			active_fields = 1,
			resource_strength = 1.				# set to 1, otherwise more resource will increase md due to larger pressure gradients
		)
		L_adv = get_L_adv(conductivity)
		p = get_pressure(
			L_adv = L_adv,
			solution = temp_solution,
			center_Agent = center_Agent,
		)
		push!(MDs, sum(abs.(p .- p[source]))/(length(E_field) - 1))	# L*L - 1 because sourc itself is not included
	end
	return mean(MDs)
end

function gamma_t(gamma_sweep,gamma_0,gamma_end,t, max_steps, phase2)
	if gamma_sweep
		 gamma = gamma_0 + (gamma_end - gamma_0)*(t[end]/max_steps)
		 if phase2
			 gamma=gamma_end
		 end
	else
		gamma = gamma_0
	end
	return gamma
end

#TODO: reactivation of agents needs to be location depended, inital conductivity needs to be adjustable
"""
    update_connections([ Adjacency_Matrix, conductivity, dead])

Updates connections between agents depending on live-state. New connections will be set to random values drawn from a log-normal distribution.
"""
function update_connections(;
    Adjacency_Matrix = Adjacency_Matrix,
    conductivity = conductivity,
    dead = dead,
)
    # sort (dis-/)connected agents into two groups
    connected_agents = findall(x -> x > 0, sum(conductivity, dims = 1)[:])
    disconnected_agents = findall(x -> x == 0, sum(conductivity, dims = 1)[:])

    # find active and disconnected agents -> reconnect them
    to_reconnect = falses(length(dead))
    to_reconnect[disconnected_agents] .= true
    to_reconnect = to_reconnect .& .!dead
    to_reconnect = findall(x -> x == true, to_reconnect)
    for i = 1:length(to_reconnect)
        conductivity[to_reconnect[i], :] .=
            0.005 * 0.5 * 2 .^ randn(length(dead)) .* Adjacency_Matrix[to_reconnect[i], :]
        conductivity[:, to_reconnect[i]] .=
            0.005 * 0.5 * 2 .^ randn(length(dead)) .* Adjacency_Matrix[:, to_reconnect[i]]
    end


    # find inactive and connected agents -> disconnect them
    connected_agents = findall(x -> x > 0, sum(conductivity, dims = 1)[:])
    to_disconnect = falses(length(dead))
    to_disconnect[connected_agents] .= true
    to_disconnect .= to_disconnect .& dead
    to_disconnect = findall(x -> x == true, to_disconnect)
    for i = 1:length(to_disconnect)
        conductivity[to_disconnect[i], :] .= 0
        conductivity[:, to_disconnect[i]] .= 0
    end

	# make sure capapcities stay symmetric (FIXME: find out why it gets assymetric in the first place)
	conductivity[:,:] = Symmetric(conductivity)[:,:]
end


"""
    update_conductivity([Adjacency_Matrix,  conductivity, p, dt, gamma, mu, direct_noise, direct_noise_strength])

Changes the capcities of transport channels accordingly to flows taking place.
"""
function update_conductivity(;
    Adjacency_Matrix = Adjacency_Matrix,
    conductivity = conductivity,
    p = p,
	dead = dead,
    dt = dt,
    gamma = 1.,
    mu = 1.,
	k=1.,
    direct_noise = false,
    direct_noise_strength = nothing,
)
	@assert !(direct_noise && direct_noise_strength==nothing)

    difference = get_difference(conductivity, p)

    Q = abs.( conductivity .* difference)

	dc = (k*(Q .^ (gamma) ./ (1 .+ (Q .^ (gamma)))) - (mu * conductivity))
    if direct_noise
        noise_matrix = direct_noise_strength * (Symmetric(randn(size(Adjacency_Matrix))) .* ceil.(conductivity))
        dc = dc + noise_matrix
	end
	conductivity[:, :] = conductivity + dc*dt
	# reflecting boundary
	conductivity[:,:] = abs.(Symmetric(conductivity))

	conductivity =conductivity .* (1 .- dead) .* transpose(1 .- dead)

end


"""
    get_L_adv(conductivity)

Calculates advection laplacian.
"""
function get_L_adv(conductivity)
    L_adv = zeros(size(conductivity))
    L_adv = (Diagonal(sum(conductivity, dims = 2)[:]) - conductivity)
    return L_adv
end


# TODO: Taketaks an state into account (no difference if dead agent is evolved, no difference between two miner agents)
"""
    get_difference(conductivity, p)

Calculates a matrix containing the p differences between all connected sides.
"""
function get_difference(conductivity, p)
    P = Array{Float64,2}(undef, length(p), length(p))                 # Matrix holding all pressure values
    P[:, :] .= p
    difference = (P - transpose(P)) .* ceil.(0.9 * conductivity / maximum(conductivity))
    return difference
end


"""
    add_noise_field()

Adds small amounts of additional resources at a random positions on the grid (also on resource patches).
The amount of additional resource patches is equal (not smaller or equal) to the given fraction.
"""
function add_noise_field(;
    sol=sol,
    fraction=noise_area,
    strength=strength,
    dead=dead,
)

    @assert fraction <= 1.

    sol = (sol - abs.(sol))/2

    modified_sol = zeros(length(sol))
    noise_patches = Integer(floor(fraction * length(sol)))
    total_inflow = sum(abs.(sol))

    patches = findall(x -> x == 0, sol)
	@assert fraction < 1

	additional_sources = sample(patches, noise_patches, replace = false)
	modified_sol[additional_sources] .= -strength
	modified_sol[additional_sources] = modified_sol[additional_sources] .* (1 .- dead[additional_sources])
	modified_sol = modified_sol + sol


    total_flow = -sum(modified_sol)
    sinks = zeros(length(sol))
    actives = findall(x -> x == false, dead)
    sinks = actives[findall(x -> x == 0, modified_sol[actives])]
    modified_sol[sinks] .= total_flow/length(sinks)

    return modified_sol
end


"""
    set_sol(source_patches,dead, [active_fraction, resource_strength])

Sets constraints for the flow out of the source and into the sink.
Sources is a vector of length E_fiel representing the inflow into the transportation network.

"""
function set_sol(;
    source_patches = E_field,
    dead = dead,
    active_fields = 1.,
	resource_strength = 1.0
)

    sol = zeros(size(source_patches))
    inflow = source_patches .* ( 1 .- dead)
    total_inflow = sum(inflow)

	@assert active_fields >= 1

    if active_fields > sum(ceil.(inflow))
        @warn "more active fields given than available setting active fields to " Integer(sum(ceil.(inflow)))
        active_fields = Integer(sum(ceil.(inflow)))
    end

    # time variant resoruce Field
    # FIXME: either add a function that calculates the pressure if all sources are available or split this function into two
    patches = findall(x -> x == 1, inflow)
    # FIXME: refine the temporal variations of the resource field
    sources = sample(patches, active_fields, replace = false)

    sol[sources] .= -1
    total_flow = -sum(sol)
    sinks = zeros(length(sol))
    actives = findall(x -> x == false, dead)
    sinks = actives[findall(x -> x == 0, sol[actives])]
    sol[sinks] .= total_flow/length(sinks)

    if sum(sol) != 0
        # FIXME: avoid printing this warning multiple times
        # @warn "flow balance is: " sum(sol)
        @assert abs(sum(sol)) < 0.001 * sum(abs.(sol)) / 2 #less than 0.1% difference
    end
    return (sol * resource_strength)
end

"""
    get_pressure(L_adv, solution)

Calculates a pressure for each lattice side.
"""
function get_pressure(; L_adv = L_adv, solution = solution, center_Agent = center_Agent)
    reduced_L_adv = hcat(L_adv[:, 1:center_Agent-1], L_adv[:, center_Agent+1:end]) #delete a the coloumn of the advection matrix that does not contributes to the flows
    p = -reduced_L_adv \ solution
	insert!(p, center_Agent, 0)
	p = p .- minimum(p)			# avoid negative pressure
    return p
end


"""
  get_pressure_profile([Adjacency_Matrix, E_field])

Calculates Pressure Profile for a given E_field for homogenious flow.
"""
function get_pressure_profile(;
	N=N,
	dead=dead,
	Adjacency_Matrix=Adjacency_Matrix,
	E_field=E_field,
	resource_strength=resource_strength,
	center_Agent=center_Agent,
	args...
	)
    L_adv = get_L_adv(Adjacency_Matrix .* (1 .- dead) .* transpose(1 .- dead))
	if Integer(sum(ceil.(E_field))) >= length(E_field)
		return zeros(length(E_field))
	end
	sol = set_sol(
        source_patches = E_field,
        dead = dead,
        active_fields = Integer(sum(ceil.(E_field))),
		resource_strength= resource_strength
    )
    p = get_pressure(L_adv = L_adv, solution = sol, center_Agent = center_Agent)
    return p
end

end
