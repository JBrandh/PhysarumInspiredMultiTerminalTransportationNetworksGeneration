include("CreateResults.jl")
include("Functions.jl")
include("helpers.jl")
include("Initaisation.jl")
include("Mechanics.jl")
include("Plotting.jl")

using DelimitedFiles
using LinearAlgebra
using Logging
using Plots
using StatsPlots


gr()

path=ARGS[1]

include(path*"parameter_file.jl")

output_path="output/"

evo_params=Dict()                               # for hyperparameters
vars = Dict()                                   # for stuff that changes

evo_params[:rec] = false                        # take snapshots of evolutionary state
evo_params[:additional_readouts] = false        # record evolution of each conductivity, pressure differecnces etc.
evo_params[:draw_path] = true                   # draw conductivities in snpashots
evo_params[:allow_eary_stop] = false            # allow stopping bevor max generations are reached


E_field = readdlm(path*"E_list")[:,1]
evo_params[:E_field] =  E_field
evo_params[:resource_abundance]  = sum(E_field) / length(E_field)


## hyperparameters

evo_params[:E_noise_field] = true
evo_params[:direct_noise] = true
evo_params[:dt] = 0.001
evo_params[:resource_abundance] = 0.0

evo_params[:N] = Int(length(E_field))
evo_params[:L] = Int(sqrt(length(E_field)))
evo_params[:center_Agent] = Int(floor((evo_params[:N] + 1) / 2))
evo_params[:Adjacency_Matrix] = externals.init_Adjacency(evo_params[:L], evo_params[:N])

evo_params[:gamma_sweep] = false
evo_params[:gamma_end] = 2.0

rescale=ParameterFile.loadFile(evo_params)


evo_params[:run_id]=externals.init_dir(evo_params, output_path)



vars[:dead] =  falses(evo_params[:N])
pressure_profile = model_mechanics.get_pressure_profile(;evo_params..., vars...)
Plotting.scatter_Energy(pressure_profile)
name = string("$output_path/$(evo_params[:run_id])/E_field_gradient.png")
savefig(name)


## Init Capacity Matrix
# WARNING: this will also connect dead agents if they exist
vars[:conductivity] = externals.init_conductivities(;evo_params...)

# Setting up activity state
vars[:p] = zeros(evo_params[:N])
dead = falses(evo_params[:N])
mask = readdlm(path*"mask")[:,1]
vars[:dead] = readdlm(path*"mask")[:,1]
pressure_profile = model_mechanics.get_pressure_profile(;evo_params..., vars...)

Plotting.scatter_Energy(pressure_profile)
name = string("$(output_path)$(evo_params[:run_id])/constrained_pressure_profile.png")
savefig(name)

Plotting.scatter_Agents(evo_params[:E_field],dead)
name = string("$(output_path)$(evo_params[:run_id])/masked_Efield.png")
savefig(name)

mask[:] .= vars[:dead]
vars[:mask] = mask

evo_params[:E_field][findall(x -> x==true,mask)] .= 0

# Disconnect dead agents
vars[:conductivity] = vars[:conductivity] .* (1 .- vars[:dead]) .* transpose(1 .- vars[:dead])


vars[:i] = 1
vars[:t] = [0.]
conductivity_history = vars[:conductivity][findall(x-> x !=0 , triu(evo_params[:Adjacency_Matrix]))]    #
vars[:conductivity_history] = conductivity_history

evo_params[:runs_per_value];
length(evo_params[:scanning_values])


labels = Vector{String}()

for value in evo_params[:scanning_values]
  label = string(evo_params[:scanning_parameter]) * " = " * string(value)
  push!(labels, label)
end

labels=permutedims(labels)

##
@info "start evolution"
if evo_params[:scan]
  helpers.scan(evo_params=evo_params, vars=vars,rescale= rescale, output_path=output_path)
  CreateResults.create_plots_from_dir("output/"*evo_params[:run_id])
  externals.plot_measures_sd("output/"*evo_params[:run_id]*"/", labels)
  if evo_params[:runs_per_value]>1
    externals.plot_boxplot("output/"*evo_params[:run_id]*"/",length(evo_params[:scanning_values]),evo_params[:runs_per_value], labels)
  end
else
  results = model_mechanics.evolve(;evo_params..., vars...)
  @info "saving results"
  CreateResults.write_final_vars(vars, evo_params[:run_id], path=output_path)
  CreateResults.write_results(results, evo_params[:run_id], path=output_path)
  CreateResults.create_plots(vars, results, evo_params, path="$(output_path)$(evo_params[:run_id])")
end


@info "done"
