module CreateResults

using Plots
using Logging
using JLD2
using FileIO
using Statistics

include("Functions.jl")
include("Plotting.jl")
include("Mechanics.jl")

function plot_mean_sd(path, size_of_scan, number_per_scan, labels; targets=["mean_distance", "normalised_conductivity", "source_pressure", "total_flows"], dpi=320, thickness_scaling=1.25, yscale=:identity, formatter=identity , fmt="png")

  for target in targets
    plot()
    output=externals.load_scan(path, target)
    if yscale == :log10
      smallest_value = floor(minimum(minimum.(output)),sigdigits=1)
      yticks_max = ceil(maximum(maximum.(output)),sigdigits=1)
      yticks = collect(range(0,stop=yticks_max, length=11))#collect(0:smallest_value:yticks_max)
      yticks = round.(yticks, sigdigits=3)
    else
      yticks=:auto
    end

    new_output=reshape(output,(number_per_scan, size_of_scan))

    if target == "source_pressure"
      new_output=reshape(mean.(new_output[:][:],dims=2),(number_per_scan, size_of_scan))
    end

    for i=1:size_of_scan
      xmax=length(new_output[1,i])
      if target=="mean_distance"
        x = collect(1:xmax)*25
        xmax = xmax*25
      else
        x = collect(1:xmax)
      end
      plot!(x,mean(new_output[:,i]),ribbon=std(new_output[:,i]),fillalpha=.4 ,label=labels[i], ylabel=titlecase(replace(target, "_" => " ")), xlabel="Generations",ylim=(-Inf, Inf), yticks=yticks, xlim=(0,xmax+1) , thickness_scaling=thickness_scaling, dpi=dpi, yscale=yscale, formatter=formatter)
    end
    savefig("$path/$target.$fmt")
  end
end

function plot_custom_scan(path, labels; targets=["mean_distance", "normalised_conductivity", "source_pressure", "total_flows"], dpi=320, thickness_scaling=1.25, yscale=:identity, formatter=identity , fmt="svg")
  for target in targets
    output=externals.load_scan(path, target)
    xmax=length(output[1])
    if yscale == :log10
      smallest_value = floor(minimum(minimum.(output)),sigdigits=1)
      yticks_max = ceil(maximum(maximum.(output)),sigdigits=1)
      yticks = collect(range(0,stop=yticks_max, length=11))#collect(0:smallest_value:yticks_max)
      yticks = round.(yticks, sigdigits=3)
    else
      yticks=:auto
    end

    if target=="mean_distance"
      x = collect(1:xmax)*25
      xmax = xmax*25
      plot(x,output, labels=labels, ylabel=titlecase(replace(target, "_" => " ")), xlabel="Generations",ylim=(-Inf, Inf), yticks=yticks,  xlim=(0,xmax) , thickness_scaling=thickness_scaling, dpi=dpi, yscale=yscale, formatter=formatter)
    else
      plot(output, labels=labels, ylabel=titlecase(replace(target, "_" => " ")), xlabel="Generations",ylim=(-Inf, Inf), yticks=yticks, xlim=(0,xmax+1) , thickness_scaling=thickness_scaling, dpi=dpi, yscale=yscale, formatter=formatter)
    end
    if target=="total_flows"
       plot(output, labels=labels, ylabel=titlecase(replace(target, "_" => " ")), xlabel="Generations",ylim=(1.25, Inf), xlim=(0,xmax+1) , thickness_scaling=thickness_scaling, dpi=dpi, yscale=:identity, formatter=formatter)
    end
    savefig("$path/$target.$fmt")
  end
end


function create_plots_from_dir(dir)
  paths = []
  for path in walkdir(dir)
    if occursin("run_", path[1][end-6:end])
      push!(paths, path[1])
    end
  end
  vars_to_read = [:conductivity, :dead, :t]
  results_to_read = [:conductivity_history, :mean_distance, :normalised_conductivity, :source_pressure, :total_flows]

  for path in paths
    vars = Dict()
    results = Dict()
    evo_params = externals.load_evo_params(path)
    for var in vars_to_read
      vars[var] = externals.load_scan(path, string(var))[1]
    end
    for result in results_to_read
      results[result] = externals.load_scan(path, string(result))[1]
    end
    @info "ploting results from $path"
    create_plots(vars, results, evo_params, path=path)

  end


end

function create_plots(vars, results, evo_params;path=nothing, thickness_scaling=1.25, dpi=320, fmt="svg")
  if path==nothing
    path = "output/$(evo_params[:run_id])"
  end
  for (key, result) in results
    if key ∈ [:final_pressures]
      continue
    end
    #TODO: find a way to calibrate y axis

    # FIXME: dont't use evo_params[:run_id], results might heve been moved or renamed
    if key == :mean_distance
      x = collect(1:length(result))*25
      plot(x,result, legend=false, ylabel=titlecase(replace("$key", "_" => " ")), xlabel="Generations",  xlim=(0,vars[:t][end]+1) , thickness_scaling=thickness_scaling, dpi=dpi)
      savefig("$path/$key.$fmt")
    else
      plot(result, legend=false, ylabel=titlecase(replace("$key", "_" => " ")), xlabel="Generations",  xlim=(0,vars[:t][end]+1) , thickness_scaling=thickness_scaling, dpi=dpi)
      savefig("$path/$key.$fmt")
    end
    closeall()
  end

  vars[:conductivity] = vars[:conductivity] .* (1 .- vars[:dead]) .* transpose(1 .- vars[:dead])
  L_adv = model_mechanics.get_L_adv(vars[:conductivity])
  stationary_flow = model_mechanics.set_sol(
  source_patches = evo_params[:E_field],
  dead = vars[:dead],
  active_fields = Integer(floor(sum(evo_params[:E_field]))),
  resource_strength = 1
  )
  #stationary_flow = model_mechanics.get_total_solution(evo_params[:E_field])
  p=model_mechanics.get_pressure(L_adv=L_adv, solution = stationary_flow, center_Agent=evo_params[:center_Agent])
  Plotting.scatter_Energy(p)
  savefig("$path/network_pressure.png")
  closeall()
  Plotting.scatter_Agents(evo_params[:E_field], vars[:dead])
  Plotting.flow_path(
    evo_params[:Adjacency_Matrix],
    vars[:conductivity],
    evo_params[:L];
    overlay = true,
    normalize_conductivity=true,
    normalisation_factor=0.4055
  )
  savefig("$path/Final_State_of_Agents.png")
  closeall()
  Plotting.flow_path(
    evo_params[:Adjacency_Matrix],
    vars[:conductivity],
    evo_params[:L];
    overlay = false,
    normalize_conductivity=true,
    #normalisation_factor=0.4055
  )
  savefig("$path/path.png")
  closeall()
end

function write_final_vars(vars, run_id; path="outputs/")
  for (key, value) in vars
    if "$key" ∉ ["i", "p"]
      externals.create_file("$path$run_id/$key", value)
    end
  end
end

function write_results(results, run_id; path="outputs/")
  for (key, value) in results
    if "$key" ∉ [""]
      externals.create_file("$path$run_id/$key", value)
    end
  end
end

end  # module
