module helpers

using Dates
using LinearAlgebra
using DelimitedFiles
using Statistics
using Distributed
using Distributions
using Plots
using Printf
#using PyPlot

include("Mechanics.jl")
include("Functions.jl")
include("Plotting.jl")
include("CreateResults.jl")

function is_connected(conductivity, dead)

  adjacency_matrix = ceil.(conductivity ./ maximum(conductivity))
  visited = falses(length(dead))

  index = sample(findall(x -> x==false, dead))                # randomly selected agent as starting pint
  visit_neighbours(index, adjacency_matrix, visited)
  visted_actives = dead+visited
  if false in visted_actives
    return false
  else
    return true
  end
end

function visit_neighbours(index, adjacency_matrix, visited)
  if visited[index]
    return false
  else
    visited[index]=true
  end
  neighbours = findall(x-> x == 1., adjacency_matrix[index,:])
  if false in visited[neighbours]
    for neighbour in neighbours
        visit_neighbours(neighbour, adjacency_matrix, visited)
    end
  end
end

function plot_mean_path(dir)
  paths = []
  for path in walkdir(dir)
    if !occursin("/run_",path[1])
      if occursin("/scan_",path[1])
        push!(paths,path[1])
      end
    end
  end
  evo_params = externals.load_evo_params("$(paths[1])/run_01")
  for path in paths
     mean_cap, cap_var = get_mean_path(path)
     Plotting.flow_path(evo_params[:Adjacency_Matrix],mean_cap,evo_params[:L],plot_var=true, path_var=cap_var, normalize_conductivity=true)
     savefig("$path/mean.png")
   end
end

"""
    get_mean_path(dir)

Calculates the mean paths of all evolved transportation network located in a given directory.
"""
function get_mean_path(dir)
    files=readdir(dir)
    caps=[]
    for file in files
      if occursin("run",file)
        read_file=readdlm("$dir/$file/conductivity",',')
        push!(caps,read_file)
      end
    end
    mean_cap = mean(caps)
    cap_var = var(caps)
    return mean_cap, cap_var
end


"""
    scan(;evo_params, vars, rescale, output_path)

Performs parameters scans.
"""
function scan(;evo_params, vars, rescale=true, output_path="output/")

    default_params = deepcopy(evo_params)
    default_vars = deepcopy(vars)

    base_id=string(evo_params[:run_id])
    scan_values = evo_params[:scanning_values]
    scanning_steps= length(evo_params[:scanning_values])
    scanning_dicts=[]

    for i=1:scanning_steps
      evo_params_scan = Dict()
      vars_scan = Dict()

      evo_params_scan = deepcopy(default_params)
      evo_params_scan[evo_params[:scanning_parameter]]=scan_values[i]
      if rescale
        @info "rescaling"
        evo_params_scan[:resource_strength] = evo_params_scan[:resource_strength]/evo_params_scan[:active_fraction]
        evo_params_scan[:max_steps] = default_params[:max_steps]/evo_params_scan[:mu]
        evo_params_scan[:k] = evo_params_scan[:mu]

        @info evo_params_scan[:resource_strength]
      end
      # WARNING: this changes resource to fix resource densitiy


      mkdir(string("$output_path", string(base_id, "/scan_", @sprintf("%02d",i))))
      externals.create_log_file(sort(evo_params_scan), string(base_id, "/scan_", @sprintf("%02d",i)), output_path=output_path)

        for j=1:evo_params[:runs_per_value]
        #  @info "running scan $((i-1)*evo_params[:runs_per_value]+j)/$(scanning_steps*evo_params[:runs_per_value])"
          # resetting changed stuff
        evo_params_scan_run = deepcopy(evo_params_scan)

        ##vars_scan = deepcopy(default_vars)
        ##results = Dict()

        ##@info "run $(evo_params[:scanning_parameter]) = $(scan_values[i])"

          scan_run_id=string(base_id, "/scan_", @sprintf("%02d",i),"/run_",@sprintf("%02d",j))

          mkdir("$output_path$scan_run_id")
          evo_params_scan_run[:run_id]=scan_run_id
          externals.write_evo_params(evo_params_scan_run, "$output_path$scan_run_id")

          # run evolution
        ##results_scan = model_mechanics.evolve(;evo_params_scan..., vars_scan...)

          push!(scanning_dicts,evo_params_scan_run)
        ##CreateResults.write_final_vars(vars_scan, scan_run_id)
        ##CreateResults.write_results(results_scan, scan_run_id)
          #CreateResults.create_plots(vars_scan, results_scan, evo_params_scan)

          #@info "saving results"

        end
    end

    # NOTE: this loop can be parallelised
    for scan in scanning_dicts

      vars_scan = deepcopy(default_vars)
      results = Dict()
      vars_scan[:conductivity] = externals.init_conductivities(;scan...)
      vars_scan[:conductivity] = vars_scan[:conductivity] .* (1 .- vars_scan[:dead]) .* transpose(1 .- vars_scan[:dead])


      @info "run $(scan[:scanning_parameter]) = $(scan[scan[:scanning_parameter]])"
      results_scan = model_mechanics.evolve(;scan..., vars_scan...)

      CreateResults.write_final_vars(vars_scan, scan[:run_id], path=output_path)
      CreateResults.write_results(results_scan, scan[:run_id], path=output_path)
    end


    #CreateResults.create_plots_from_dir("output/$base_id")
    @info "scan done"
end

end
