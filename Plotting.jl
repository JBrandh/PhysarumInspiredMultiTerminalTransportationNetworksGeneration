module Plotting

using LinearAlgebra
using Statistics
using Plots
#using PyPlot
using Distributed

gr()
#plotlyjs()
#pyplot()



export scatter_Agents

function scatter_Energy(E_field; mode = "field", legend = false, dpi = 320)
    L = sqrt(length(E_field))
    ToPlot = zeros(Float64, length(E_field), 3)

    for i = 1:length(E_field)
        x = (i - 1) % L
        y = - (i - ((i - 1) % L))/L * 0.5 * sqrt(3)
        x -= (9/16) * y                             #16/9 aspect ratio

        ToPlot[i, 1] = x
        ToPlot[i, 2] = y
        ToPlot[i, 3] = E_field[i]
    end
    if mode == "field"
        fig = scatter(
            ToPlot[:, 1],
            ToPlot[:, 2],
            marker_z = ToPlot[:, 3],
            color = :lighttest,
            markershape = :hexagon,
            markersize = 220 / L,               # best for 15x15
            label = "Resouces Richness",
            legend = legend,
            dpi = dpi,
            aspect_ratio=1,
            axis=nothing,
            border=:none
        )
        plot(fig)
    elseif mode == "agents"
        fig = scatter(
            ToPlot[:, 1],
            ToPlot[:, 2],
            marker_z = ToPlot[:, 3],
            color = :lighttest,
            markershape = :hexagon,
            markersize = 220 / L ,
            label = "Agents Energy",
            legend = legend,
            dpi = dpi,
            aspect_ratio = 1,
            axis=nothing,
            border=:none
        )
        plot(fig)
    else
    @warn "mode has to be 'agents' or 'field'"
    end

end

function scatter_Agents(E_field, dead; legend = false , dpi=320)


    L = sqrt(length(dead))
    ToPlot = zeros(Float64, length(dead), 3)

    dead = dead
    ToPlot_Dead = zeros(Float64, sum(dead), 2)

    j = 1
    for i = 1:length(dead)
        x = (i - 1) % L
        y = - (i - ((i - 1) % L))/L * 0.5 * sqrt(3)
        x -= (9/16) * y                             #16/9 aspect ratio

        ToPlot[i, 1] = x
        ToPlot[i, 2] = y
        ToPlot[i, 3] = E_field[i]

        if dead[i]
            ToPlot_Dead[j, 1] = x
            ToPlot_Dead[j, 2] = y
            j = j + 1
        end

    end

    fig = scatter(
        ToPlot[:, 1],
        ToPlot[:, 2],
        marker_z = ToPlot[:, 3],
        color = :lighttest,
        markershape = :hexagon,
        markersize = 220 / L ,
        label = "Agents",
        legend = legend,
        dpi = dpi,
        axis=nothing,
        border=:none
    )
    scatter!(
        ToPlot_Dead[:, 1],
        ToPlot_Dead[:, 2],
        markercolor = :black,
        markershape = :hexagon,
        markersize = 220 / L ,
        label = "Agents",
        legend = legend,
        dpi = dpi,
        aspect_ratio=1,
        axis=nothing,
        border=:none
    )
    plot(fig)
end

function flow_path(Adjacency_Matrix, conductivity, L; plot_var=false, path_var=[], overlay = false, normalize_conductivity=false, normalisation_factor=maximum(conductivity), root=true, dpi=320)

    if !overlay
        plot()
    end

    # NOTE: order of normalising and taking the root does matter
    variances = zeros(size(conductivity))
    if plot_var
        variances = zeros(size(path_var))
        variances[:,:] .= path_var[:,:]

        sd = sqrt.(variances)
        cv = sd ./ conductivity
        # removing NaNs after dividing by zero by zero
        replace!(cv, NaN=>0)
        # remove links with very low conductivity as they tend to have misleadingly high fluctionations and hence distract from more important parts of the network
        cv[findall(x -> x<maximum(conductivity)/10000, conductivity)] .= 0

        cv_scaled = cv .- minimum(cv)
        cv_scaled = cv / maximum(cv)

        heatmap!(
        [maximum(cv)/2 maximum(cv)/2; maximum(cv)/2 maximum(cv)/2],
        c=cgrad([:blue, :white, :red]),
        clims=(minimum(cv), maximum(cv)),
        dpi = dpi,
        aspect_ratio=1,
        axis=nothing,
        border=:none,
        colorbar=:bottom,
        legendfontrotation=180,
        colorbar_title="CV"
        )
    end



    if normalize_conductivity
        conductivity = 3.0 * conductivity / normalisation_factor
    end

    if root
        conductivity = conductivity.^(1/4)
    end

    tl = 2 * pi * 330 / 360.0    # top left
    tr = 2 * pi * 30 / 360      # top right
    l = 2 * pi * 270 / 360      # left
    r = 2 * pi * 90 / 360       # right
    bl = 2 * pi * 210 / 360     # buttom left
    br = 2 * pi * 150 / 360     # buttom right



    # TODO this is not slow but ugly
    for i = 1:(L*L)
        x = (i - 1) % L
        y = - (i - ((i - 1) % L))/L * 0.5 * sqrt(3)
        x -= (9/16) * y                             #16/9 aspect ratio

        specialization = zeros(6)          # distribution of uptake directions
        local_var = zeros(6)

        n = sum(ceil.(Adjacency_Matrix[:, i]))      # number of neighbors

        tl_component = try                                    # try accesing conductivity [i,i-L] if it does not exist set to zero
            0.5 * ceil(conductivity[i-L, i]) * [sin(tl), cos(tl)]
        catch
            [0, 0]
        end
        specialization[1] = try
            conductivity[i-L, i]
        catch
            0
        end
        local_var[1] = try
            cv_scaled[i-L, i]
        catch
            0
        end

        tr_component = try
            0.5 * ceil(conductivity[i-L+1, i]) * [sin(tr), cos(tr)]
        catch
            [0, 0]
        end
        specialization[2] = try
            conductivity[i-L+1, i]
        catch
            0
        end
        local_var[2] = try
            cv_scaled[i-L+1, i]
        catch
            0
        end

        l_component = try
            0.5 * ceil(conductivity[i-1, i]) * [sin(l), cos(l)]
        catch
            [0, 0]
        end
        specialization[3] = try
            conductivity[i-1, i]
        catch
            0
        end
        local_var[3] = try
            cv_scaled[i-1, i]
        catch
            0
        end

        r_component = try
            0.5 * ceil(conductivity[i+1, i]) * [sin(r), cos(r)]
        catch
            [0, 0]
        end
        specialization[4] = try
            conductivity[i+1, i]
        catch
            0
        end
        local_var[4] = try
            cv_scaled[i+1, i]
        catch
            0
        end

        bl_component = try
            0.5 * ceil(conductivity[i+L-1, i]) * [sin(bl), cos(bl)]
        catch
            [0, 0]
        end
        specialization[5] = try
            conductivity[i+L-1, i]
        catch
            0
        end
        local_var[5] = try
            cv_scaled[i+L-1, i]
        catch
            0
        end

        br_component = try
            0.5 * ceil(conductivity[i+L, i]) * [sin(br), cos(br)]
        catch
            [0, 0]
        end
        specialization[6] = try
            conductivity[i+L, i]
        catch
            0
        end
        local_var[6] = try
            cv_scaled[i+L, i]
        catch
            0
        end

        components = hcat([
            tl_component,
            tr_component,
            l_component,
            r_component,
            bl_component,
            br_component,
        ])

        #FIXME: this is slow

        for j = 1:6
            if plot_var
                plot!(
                    [x, x + components[j][1]],
                    [y, y + components[j][2]],
                    linealpha = 0.85,
                    linewidth = 8 * specialization[j],
                    color = RGB(0+local_var[j], 0.5 - abs(local_var[j])/2, 1-local_var[j]),      # For rGb value [..., 0.5 - abs(local_var[j])/2, ...]
                    colorbar=true,
                    legend = false,
                    dpi = dpi,
                    aspect_ratio=1,
                    axis=nothing,
                    border=:none
                )
            else
                plot!(
                    [x, x + components[j][1]],
                    [y, y + components[j][2]],
                    linealpha = 0.85,
                    linewidth = 8 * specialization[j],
                    color = "grey",
                    legend = false,
                    dpi = dpi,
                    aspect_ratio=1,
                    axis=nothing,
                    border=:none
                )
            end
        end

    end

    plot!(legend = false)

end

end
