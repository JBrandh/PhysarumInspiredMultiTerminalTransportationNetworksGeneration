
module externals

using Dates
using DelimitedFiles
using Formatting
using LinearAlgebra
using Statistics
using StatsBase
using Plots
using Printf
using RollingFunctions
using JLD2
using FileIO

include("Plotting.jl")

function plot_boxplot(data_path, size_of_scan, number_per_scan, labels; targets=["normalised_conductivity", "source_pressure", "total_flows", "mean_distance"],add_baseline=false, baseline_path="", thickness_scaling=1.25, yscale=:identity, formatter=identity, yformatter=x-> "$(round(x, sigdigits=3))" , dpi=320, fmt="svg")
  for target in targets
    data = create_boxplot_data(data_path, size_of_scan, number_per_scan, target)
    smallest_value = floor(minimum(minimum.(data)),sigdigits=1)
    if yscale == :log10
      yticks_max = ceil(maximum(maximum.(data)),sigdigits=1)
      yticks = collect(range(smallest_value,stop=yticks_max, length=21))#collect(0:smallest_value:yticks_max)
      yticks = round.(yticks, sigdigits=3)
    else
      yticks=:auto
    end

    boxplot(labels, yticks=yticks, data, legend=false, color="grey", thickness_scaling=thickness_scaling, ylabel=titlecase(replace(target, "_" => " ")), dpi=dpi, yscale=yscale, formatter=yformatter=x-> "$(round(x, sigdigits=3))")
    if target=="total_flows"
       boxplot(labels, data, legend=false, color="grey", thickness_scaling=thickness_scaling, ylabel=titlecase(replace(target, "_" => " ")), dpi=dpi, yscale=:identity, formatter=yformatter=x-> "$(round(x, sigdigits=3))")
    end

    if add_baseline
      baseline_data = create_boxplot_data(baseline_path, size_of_scan, 1, target)
      boxplot!(labels, baseline_data, color="red", yticks=yticks, linewidth=1, linecolor="red", yscale=yscale,formatter=formatter)
    end
    savefig("$data_path/$(target)_boxplot.$fmt")
  end
end

function plot_distance_from_optimum(data_path,size_of_scan, number_per_scan, labels, optimum; target="total_flows", dpi=320, thickness_scaling=1.25, yscale=:identity, fmt="svg")

    data = create_boxplot_data(data_path, size_of_scan, number_per_scan, target)
    for set in data
      set[:] = set[:] .- optimum
    end

    boxplot(labels, data, legend=false, color="grey", yformatter=x-> "$(round(x/10^round(log10(x)),digits=3))e$(round(log10(x)))", thickness_scaling=thickness_scaling, ylabel=titlecase(replace(target, "_" => " ")), dpi=dpi, yscale=yscale)

    savefig("$data_path/$(target)_distance_to_optimum.$fmt")
end

function plot_violinplot(data_path, size_of_scan, number_per_scan, labels; targets=["normalised_conductivity", "source_pressure", "total_flows", "mean_distance"],add_baseline=false, baseline_path="", yscale=:identity, dpi=320, fmt="svg")
  for target in targets
    data = create_boxplot_data(data_path, size_of_scan, number_per_scan, target)

    violin(labels, data, legend=false, color="grey", thickness_scaling=1.25, ylabel=titlecase(replace(target, "_" => " ")), yscale=yscale, dpi=dpi)
    if add_baseline
      baseline_data = create_boxplot_data(baseline_path, size_of_scan, 1, target)
      violin!(labels, baseline_data, color="red", linewidth=1, linecolor="red", yscale=yscale)
    end
    savefig("$data_path/$(target)_violin.$fmt")
  end
end

function plot_cv(data_path, size_of_scan, number_per_scan, scan_values; targets=["normalised_conductivity", "source_pressure", "total_flows", "mean_distance"],add_baseline=false, baseline_path="", dpi=320, fmt="svg")
  for target in targets
    data = create_boxplot_data(data_path, size_of_scan, number_per_scan, target)

    cv=std.(data) ./ mean.(data)
    scatter(scan_values, cv, legend=false, color="grey", thickness_scaling=1.25, ylabel=titlecase(replace(target, "_" => " ")), dpi=dpi)
    if add_baseline
      baseline_data = create_boxplot_data(baseline_path, size_of_scan, 1, target)
      cv_baseline=std.(baseline_data) ./ mean.(baseline_data)
      scatter!(scan_values, cv_baseline, color="red", linewidth=1, linecolor="red")
    end
    savefig("$data_path/$(target)_cv.$fmt")
  end
end


function create_boxplot_data(base_path, size_of_scan, number_per_scan, target)
  scans=[]
  for i in 1:size_of_scan
    runs=[]
    path = base_path
    path = string(path, "scan_", @sprintf("%02d",i))

    for j in 1:number_per_scan
      data = load_scan(string(path, "/run_", @sprintf("%02d",j)), target)
      @assert length(data) > 0
      data = data[1]
      append!(runs, data[end])
    end
    push!(scans,runs)
  end
  return scans
end


function plot_measures(base_path, labels; targets=["normalised_conductivity", "source_pressure", "total_flows", "mean_distance"], legend=true, dpi=320, fmt="svg")
  linestyle = [:dashdotdot, :solid, :dash, :dot, :dashdot]
  for output in targets
    results=load_scan(base_path, output)
    plot(results, xlabel="Generations",xlim=(0, Inf), linewidth=1.8, thickness_scaling=1.25, label=labels, ylabel=titlecase(replace(output, "_" => " ")), legend=legend, dpi=dpi)
    savefig("$base_path/$output.$fmt")
  end
end

function plot_measures_sd(base_path, labels; targets=["normalised_conductivity", "source_pressure", "total_flows", "mean_distance"], dpi=320, fmt="svg")
  for output in targets
    plot()
    let i=1
      while true
        try
          scanpath = "$(base_path)scan_$(@sprintf("%02d",i))"
          results=load_scan(scanpath, output)
          mean_results = mean(results)
          sd_results = std(results)
          rolling_mean=rolling(mean, mean_results[:,1],10)
          rolling_sd=rolling(mean, sd_results[:,1],10)
          x = (5):(length(results[1])-5)
          plot!(x, rolling_mean, ribbon=rolling_sd, fillalpha=.4 ,label=labels[i], xlabel="Generations",xlim=(0, Inf), ylabel=titlecase(replace(output, "_" => " ")),legend=true, dpi=dpi)
          # ribbon=results_sd,
          i = i+1
        catch
          break
        end
      end
    end
    savefig("$base_path/$output.$fmt")
  end
end


function cat_frames(dir)
  states = dir .* "/states/" .* readdir("$dir/states/")
  pc = dir .* "/pc/" .*readdir("$dir/pc/")
  cq = dir .* "/cq/" .*readdir("$dir/cq/")
  pq = dir .* "/pq/" .*readdir("$dir/pq/")
  mkdir("$dir/output")

  # TODO: this can be parallelised
  f=open("$dir/output/frame_list.txt","w")
  for i in 1:length(states)
    filenumber = @sprintf("%04d",i)
    run(`convert $(states[i]) $(pc[i]) $(cq[i]) $(pq[i]) +append $dir/output/file$filenumber.png`)
    write(f, "file 'file$filenumber.png'\n")
  end
  close(f)

  # this allready uses multiple cores
  run(`ffmpeg -y -r 10 -f concat -safe 0 -i $dir/output/frame_list.txt -c:v libx264 -vf "fps=25,format=yuv420p" $dir/output/out.mp4`)

  return states, pc, cq
end


function create_log_file(params, run_id; output_path="output/")
  @info "init log file"
  logfile = "$output_path$run_id/logfile.log"
  log_file = open(logfile, "a")
  for item in collect(keys(params))
    if item ∉ ["Adjacency_Matrix", "E_field", "rec"]
      println(log_file, item, " = ",params[item])
    end
  end
  close(log_file)
end

function set_E_field(N,load_field, fraction_covered, run_id; output_path="output/")
  E_field = zeros(N)

  # TODO: unloop this
  E_field[sample(collect(1:N), Int(floor(N*fraction_covered)), replace=false)] .= 1.

  if load_field
    @info "searching for resource field"
    preset_found = isfile("E_list")
    if preset_found
      E_field[:] = readdlm("E_list")
      writedlm("$output_path$run_id/E_list", E_field, ',')
      @info "resource field found"
    else
      writedlm("$output_path$run_id/E_list", E_field, ',')
      @warn "No Energy Field preset found. New Field was generated."
    end
  else
    writedlm("$output_path$run_id/E_list", E_field, ',')
    @info "New Energy Field generated."
  end

  ### Generate E_field picture
  Plotting.scatter_Energy(E_field)
  name = "$output_path$run_id/E_Field_size=$(N)_fc=$fraction_covered.png"
  savefig(name)

  return E_field
end

function load_mask(dead)
  mask_found = isfile("mask")
  if mask_found
    @info "mask found"
    dead[:] = readdlm("mask")
  else
    @warn "No mask found, continuing without mask!"
  end

  return dead
end

function loadParameterFile(path,evo_params)
  open(path*"parameters") do file
    for ln in eachline(file)    
      println(split(ln, "=", keepempty=false))
    end
  end
end



function create_file(name, data)
  writedlm("$name",data,',')
end

function write_evo_params(evo_params, path)
  save("$path/evo_params.jld2", "data", evo_params)
end

function load_evo_params(path)
  load("$path/evo_params.jld2")["data"]
end


function load_file(pwd)
  data=readdlm(pwd, ',')
  return data
end


function load_scan(path, target)
  files=[]
  for file in walkdir(path)
    push!(files,file)
  end
  targets=[]
  for file in files
    if target in file[3]
      push!(targets, "$(file[1])/$target")
    end
  end
  sort!(targets)
  outputs = []
  for file in targets
    push!(outputs,load_file(file))
  end
  return outputs
end


function load_set(pwd, run_date, file; size=12)
  set=[]
  for i=1:size
       append!(set,[load_file(string(pwd,"core",i,"/output/",run_date,"/",file))])
       end
  return set
end


function init_conductivities(;
  Adjacency_Matrix=Adjacency_Matrix,
  init_random=init_random,
  resource_strength=resource_strength,
  gamma_0=gamma_0,
  mu=mu,
  k=k,
  initial_caps=initial_caps,
  args...
  )

  base_cond = resource_strength * initial_caps * k / mu


  N = Int(sqrt(length(Adjacency_Matrix)))
  conductivities = ones(Float64,  N,  N)
  if init_random
    conductivities = Adjacency_Matrix * base_cond .* (0.5 * (2 .^ randn(N,N)))
  else
    conductivities = Adjacency_Matrix * base_cond
  end
  conductivities[:,:] = Symmetric(conductivities)[:,:]
  return conductivities
end


function init_Adjacency(L,N)
  Adjacency_Matrix = zeros(Float64,N,N)

  for i=1:N
    if i<=L                           # First Row
      if i==1
        Adjacency_Matrix[i,i]=0.        # self
        Adjacency_Matrix[i,i+1]=1.      # right (r)
        Adjacency_Matrix[i,i+L]=1.      # bottom right (br)
      elseif i==L
        Adjacency_Matrix[i,i]=0.        # self
        Adjacency_Matrix[i,i-1]=1.      # left (l)
        Adjacency_Matrix[i,i+L-1]=1.    # bottom left (bl)
        Adjacency_Matrix[i,i+L]=1.      # br
      else
        Adjacency_Matrix[i,i]=0.        # self
        Adjacency_Matrix[i,i-1]=1.      # l
        Adjacency_Matrix[i,i+1]=1.      # r
        Adjacency_Matrix[i,i+L-1]=1.    # bl
        Adjacency_Matrix[i,i+L]=1.      # br
      end

    elseif i%L==1                     # First Column
      if i==1
        Adjacency_Matrix[i,i]=0.        # self
        Adjacency_Matrix[i,i+1]=1.      # r
        Adjacency_Matrix[i,i+L]=1.      # br
      elseif i==L*(L-1)+1
        Adjacency_Matrix[i,i]=0.        # self
        Adjacency_Matrix[i,i-L]=1.      # top left (tl)
        Adjacency_Matrix[i,i-L+1]=1.    # top right (tr)
        Adjacency_Matrix[i,i+1]=1.      # r
      else
        Adjacency_Matrix[i,i]=0.        # self
        Adjacency_Matrix[i,i-L]=1.      # tl
        Adjacency_Matrix[i,i-L+1]=1.    # tr
        Adjacency_Matrix[i,i+1]=1.      # tl
        Adjacency_Matrix[i,i+L]=1.      # br
      end

    elseif L*(L-1)+1<= i && i <= L*L  # Last Row
      if i== L*(L-1)+1
        Adjacency_Matrix[i,i]=0.        # self
        Adjacency_Matrix[i,i-L]=1.      # tl
        Adjacency_Matrix[i,i-L+1]=1.    # tr
        Adjacency_Matrix[i,i+1]=1.      # r
      elseif i==L*L
        Adjacency_Matrix[i,i]=0.        # self
        Adjacency_Matrix[i,i-L]=1.      # tl
        Adjacency_Matrix[i,i-1]=1.      # l
      else
        Adjacency_Matrix[i,i]=0.        # self
        Adjacency_Matrix[i,i-L]=1.      # tl
        Adjacency_Matrix[i,i-L+1]=1.    # tr
        Adjacency_Matrix[i,i-1]=1.      # l
        Adjacency_Matrix[i,i+1]=1.      # r
      end

      elseif i%L== 0                  # Last Column
        if i==L
          Adjacency_Matrix[i,i]=0.        # self
          Adjacency_Matrix[i,i-1]=1.      # l
          Adjacency_Matrix[i,i+L-1]=1.    # bl
          Adjacency_Matrix[i,i+L]=1.      # br
        elseif i==L*L
          Adjacency_Matrix[i,i]=0.        # self
          Adjacency_Matrix[i,i-L]=1.      # tl
          Adjacency_Matrix[i,i-1]=1.      # l
        else
          Adjacency_Matrix[i,i]=0.        # self
          Adjacency_Matrix[i,i-L]=1.      # tl
          Adjacency_Matrix[i,i-1]=1.      # l
          Adjacency_Matrix[i,i+L-1]=1.    # bl
          Adjacency_Matrix[i,i+L]=1.      # br
        end

      else                              # non-fringe Agents
        Adjacency_Matrix[i,i]=0.          # self
        Adjacency_Matrix[i,i-L]=1.        # tl
        Adjacency_Matrix[i,i-L+1]=1.      # tr
        Adjacency_Matrix[i,i-1]=1.        # l
        Adjacency_Matrix[i,i+1]=1.        # r
        Adjacency_Matrix[i,i+L-1]=1.      # bl
        Adjacency_Matrix[i,i+L]=1.        # br
      end
    end

  return Adjacency_Matrix
end


function print_progress(i,steps)
  if i==1
    print("Startet at: ",Dates.Time(Dates.now()),"\n")
  end

  if i/steps * 100 % 1 == 0
    print("\r|")
    for j=1:Int(ceil(i/steps * 100 / 5))-1
      print("=")
    end
    print(">")
    for j=Int(ceil(i/steps * 100 / 5)):19
      print(" ")
    end
    print("|  ",Int(ceil((i/steps)*100)),"%")
  end
  if i==steps
    print("\n","Done at: ",Dates.Time(Dates.now()),"\n")
  end
end

function init_dir(evo_params,output_path="output/")
  # TODO use @sprintf for leading zeros
  run_id = "run_$(Dates.Date(Dates.now()))_$(Dates.hour(Dates.now())):$(Dates.minute(Dates.now()))"

  mkdir("$output_path$run_id")
  logfile = "$output_path$run_id/logfile.log"
  mkdir("$output_path$run_id/states")
  mkdir("$output_path$run_id/pc")
  mkdir("$output_path$run_id/cq")
  mkdir("$output_path$run_id/pq")
  create_log_file(sort(evo_params), run_id,output_path=output_path)
  return run_id
end

end  # module
