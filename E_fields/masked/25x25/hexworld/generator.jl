L = 25
N = L * L

dead = falses(N)

for i=1:Int(L/2)-1
       dead[1+(i-1)*L:(i-1)*L+((Int(L/2)-1)-i)] .= true
end

for i=Int(L/2):L
       dead[i*L-(i-Int(L/2)):i*L] .= true
end
