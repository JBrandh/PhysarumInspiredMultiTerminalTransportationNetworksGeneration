function hex_star(n;center_Agent=center_Agent,L=L)
       E_field=zeros(L*L)
       E_field[center_Agent-n]=1
       E_field[center_Agent-(n*L)]=1
       E_field[center_Agent-(n*L)+n]=1
       E_field[center_Agent+n]=1
       E_field[center_Agent+(n*L)]=1
       E_field[center_Agent+(n*L)-n]=1
       return E_field
       end 
       
for i=1:12
       name=string("hex_star_",i)
       mkdir(name)
       E_field=hex_star(i)
       writedlm(string(name,"/E_list"), E_field, ',')
       Plotting.scatter_Energy(hex_star(i))
       savefig(string(name,"/E_field"))
       end
